function stickyMenu(){
    var sticky=document.getElementById ('sticky');
    if (window.pageYOffset > 220) {
    sticky.classList.add('sticky');
}
    else
{
    sticky.classList.remove('sticky');
}
}
    window.onscroll = function () {
    sticky
}



const display_button = document.getElementById('displayButton')
const dataset_portion_dropdown = document.getElementById('monthly-annual')
const timeframe_dropdown = document.getElementById('timeframe')
const region_dropdown = document.getElementById('region')
const province_dropdown = document.getElementById('province')
const agricultural_product_dropdown = document.getElementById('agricultural-product')
const commodity_dropdown = document.getElementById('commodity')
const visual_div = document.getElementById('visual')


display_button.onclick = display_dropdown_values
region_dropdown.onchange = dynamic_dropdown_province
agricultural_product_dropdown.onchange = dynamic_dropdoown_commodity




function test(){
    console.log('test')
}

function readTextFile(file, callback) {
    var rawFile = new XMLHttpRequest();
    rawFile.overrideMimeType("application/json");
    rawFile.open("GET", file, true);
    rawFile.onreadystatechange = function() {
        if (rawFile.readyState === 4 && rawFile.status == "200") {
             callback(rawFile.responseText);
        }
    }
    rawFile.send(null);
}


function display_dropdown_values(){
    var dataset_portion = dataset_portion_dropdown.options[dataset_portion_dropdown.selectedIndex].text
    var region = region_dropdown.options[region_dropdown.selectedIndex].text
    var province = province_dropdown.options[province_dropdown.selectedIndex].text
    var agricultural_product = agricultural_product_dropdown.options[agricultural_product_dropdown.selectedIndex].text
    var commodity = commodity_dropdown.options[commodity_dropdown.selectedIndex].value
    visual_div.innerHTML = '';
    var analysis = ['Median','Count','Mean','Standard Deviation','Min','25%','50%','75%', 'Max']
    if (dataset_portion === "Annual" || region === "" || province === "" || agricultural_product === "" || commodity === "") {
        console.log("annual-statistics/"+commodity+"/"+province+".json")
        readTextFile("annual-statistics/"+commodity+"/"+province+".json", function(text){
            var data = JSON.parse(text);
            console.log(data)
            console.log(commodity+' Descriptive Statistics in '+province)
            var str = "<table><tr><th>Descriptive Statistics of "+commodity+" in "+ province+"</th></tr>"
            var array = data[commodity+' Descriptive Statistics in '+province]
            for (let x = 0; x < array.length; x++ ) {
                 console.log(array[x])
                 if (x === (array.length-1)) {
                    str = str.concat("<tr><td>"+analysis[x]+": "+String(array[x]+"</td></tr></table"))
                 } else {
                     str = str.concat("<tr><td>"+analysis[x]+": "+String(array[x]+"</td></tr>"))
                 }
                 
            }
            visual_div.innerHTML = str
        })
        // visual_div.innerHTML = "<table><tr><th>Descriptive Statistics of "+commodity+" in "+ province+"</th></tr></table>";
        
    } else {
        visual_div.innerHTML = "<img src='new-forecast-monthly/"+commodity+"/"+province+".jpg'>"    
        readTextFile("new-accuracy-metrics-monthly/"+commodity+"/"+province+".json", function(text){
            var data = JSON.parse(text);
            console.log(data)
            console.log(Object.keys((data[commodity+' Accuracy Metrics in '+province])).length)

            var metrics = ['acf1','corr','mae','mape','me','minmax','mpe','rmse']
            var str = "<table><tr><th>Descriptive Statistics of "+commodity+" in "+ province+"</th></tr>"
            // var array = data[commodity+'-monthly Accuracy Metrics in '+province]
            var array
            if (Object.keys((data[commodity+' Accuracy Metrics in '+province])).length > 0 ) {
                array = data[commodity+' Accuracy Metrics in '+province]
                console.log('reached no monthly')
                console.log(array)
            } else {
                array = data[commodity+'-monthly Accuracy Metrics in '+province]
                console.log('reached monthly')
                console.log(array)
            }
            console.log(array)
            metrics.forEach(function(element){
                let index = metrics.indexOf(element)
                if (index === (metrics.length-1)) {
                    str = str.concat("<tr><td>"+metrics[index]+": "+String(array[element]+"</td></tr></table"))
                } else {
                    str = str.concat("<tr><td>"+metrics[index]+": "+String(array[element]+"</td></tr>"))
                }
            })
            visual_div.innerHTML += str
        })
    }
}

function removeOptions(selectElement) {
    var i, L = selectElement.options.length - 1;
    for(i = L; i >= 0; i--) {
       selectElement.remove(i);
    }
}

function dynamic_dropdown_timeframe(){
    var portion = timeframe_dropdown.options[timeframe_dropdown.selectedIndex].text
    switch(portion){
        case "Monthly":
            break
        case "Annual":
            break
    }
}

function dynamic_dropdown_province(){
    var region = region_dropdown.options[region_dropdown.selectedIndex].text
    switch (region) {
        case "CAR":
            removeOptions(document.getElementById('province'));
            document.getElementById('province').options[0] = new Option("","")
            document.getElementById('province').options[1] = new Option("Abra","abra")
            document.getElementById('province').options[2] = new Option("Apayao","apayao")
            document.getElementById('province').options[3] = new Option("Benguet","benguet")
            document.getElementById('province').options[4] = new Option("Ifugao","ifugao")
            document.getElementById('province').options[5] = new Option("Kalinga","kalinga")
            document.getElementById('province').options[6] = new Option("Mountain Province","mountain-province")
            break
        case "Region I (ILOCOS REGION)":
            removeOptions(document.getElementById('province'));
            document.getElementById('province').options[0] = new Option("","")
            document.getElementById('province').options[1] = new Option("Ilocos Norte","ilocos-norte")
            document.getElementById('province').options[2] = new Option("Ilocos Sur","ilocos-sur")
            document.getElementById('province').options[3] = new Option("La Union","la-union")
            document.getElementById('province').options[4] = new Option("Pangasinan","pangasinan")
            break
        case "Region II (CAGAYAN VALLEY)":
            removeOptions(document.getElementById('province'));
            document.getElementById('province').options[0] = new Option("","")
            document.getElementById('province').options[1] = new Option("Cagayan","cagayan")
            document.getElementById('province').options[2] = new Option("Isabela","isabela")
            document.getElementById('province').options[3] = new Option("Nueva Vizcaya","nueva-vizcaya")
            document.getElementById('province').options[4] = new Option("Quirino","quirino")
            break
        case "Region III (CENTRAL LUZON)":
            removeOptions(document.getElementById('province'));
            document.getElementById('province').options[0] = new Option("","")
            document.getElementById('province').options[1] = new Option("Aurora","aurora")
            document.getElementById('province').options[2] = new Option("Bataan","bataan")
            document.getElementById('province').options[3] = new Option("Bulacan","bulacan")
            document.getElementById('province').options[4] = new Option("Nueva Ecija","nueva-ecija")
            document.getElementById('province').options[5] = new Option("Pampanga","pampanga")
            document.getElementById('province').options[6] = new Option("Tarlac","tarlac")
            document.getElementById('province').options[7] = new Option("Zambales","zambales")
            break
        case "Region IV-A (CALABARZON)":
            removeOptions(document.getElementById('province'));
            document.getElementById('province').options[0] = new Option("","")
            document.getElementById('province').options[1] = new Option("Batangas","batangas")
            document.getElementById('province').options[2] = new Option("Cavite","cavite")
            document.getElementById('province').options[3] = new Option("Laguna","laguna")
            document.getElementById('province').options[4] = new Option("Quezon","quezon")
            document.getElementById('province').options[5] = new Option("Rizal","rizal")
            break
        case "MIMAROPA REGION":
            removeOptions(document.getElementById('province'));
            document.getElementById('province').options[0] = new Option("","")
            document.getElementById('province').options[1] = new Option("Marinduque","marinduque")
            document.getElementById('province').options[2] = new Option("Occidental Mindoro","occidental-mindoro")
            document.getElementById('province').options[3] = new Option("Oriental Mindoro","oriental-mindoro")
            document.getElementById('province').options[4] = new Option("Palawan","palawan")
            document.getElementById('province').options[5] = new Option("Romblon","romblon")
            break
        case "REGION V (BICOL REGION)":
            removeOptions(document.getElementById('province'));
            document.getElementById('province').options[0] = new Option("","")
            document.getElementById('province').options[1] = new Option("Albay","albay")
            document.getElementById('province').options[2] = new Option("Camarines Norte","camarines-norte")
            document.getElementById('province').options[3] = new Option("Camarines Sur","camarines-sur")
            document.getElementById('province').options[4] = new Option("Catanduanes","catanduanes")
            document.getElementById('province').options[5] = new Option("Masbate","masbate")
            document.getElementById('province').options[6] = new Option("Sorsogon","sorsogon")
            break
        case "Region VI (WESTERN VISAYAS)":
            removeOptions(document.getElementById('province'));
            document.getElementById('province').options[0] = new Option("","")
            document.getElementById('province').options[1] = new Option("Aklan","aklan")
            document.getElementById('province').options[2] = new Option("Antique","antique")
            document.getElementById('province').options[3] = new Option("Capiz","capiz")
            document.getElementById('province').options[4] = new Option("Guimaras","guimaras")
            document.getElementById('province').options[5] = new Option("Ilo-ilo","ilo-ilo")
            document.getElementById('province').options[6] = new Option("Negros Occidental","negros-occidental")
            break
        case "Region VII (CENTRAL VISAYAS)":
            removeOptions(document.getElementById('province'));
            document.getElementById('province').options[0] = new Option("","")    
            document.getElementById('province').options[1] = new Option("Bohol","bohol")
            document.getElementById('province').options[2] = new Option("Cebu","cebu")
            document.getElementById('province').options[3] = new Option("Negros Oriental","negros-oriental")
            document.getElementById('province').options[4] = new Option("Siquijor","")
            break
        case "Region VIII (EASTERN VISAYAS)":
            removeOptions(document.getElementById('province'));
            document.getElementById('province').options[0] = new Option("","")    
            document.getElementById('province').options[1] = new Option("Biliran","biliran")
            document.getElementById('province').options[2] = new Option("Eastern Samar","eastern-samar")
            document.getElementById('province').options[3] = new Option("Leyte","leyte")
            document.getElementById('province').options[4] = new Option("Northern Samar","northern-samar")
            document.getElementById('province').options[5] = new Option("Samar","samar")
            document.getElementById('province').options[6] = new Option("Southern Leyte","southern-leyte")
            break
        case "Region IX (ZAMBOANGA PENINSULA)":
            removeOptions(document.getElementById('province'));
            document.getElementById('province').options[0] = new Option("","")
            document.getElementById('province').options[1] = new Option("Zamboanga Del Norte","zam-del-norte")
            document.getElementById('province').options[2] = new Option("Zamboanga Del Sur","zam-del-sur")
            document.getElementById('province').options[3] = new Option("Zamboanga Sibugay","zam-sibugay")
            document.getElementById('province').options[4] = new Option("Zamboanga City","zam-city")
            break
        case "Region X (NORTHERN MINDANAO)":
            removeOptions(document.getElementById('province'));
            document.getElementById('province').options[0] = new Option("","")
            document.getElementById('province').options[1] = new Option("Bukidnon","bukidnon")
            document.getElementById('province').options[2] = new Option("Camiguin","camiguin")
            document.getElementById('province').options[3] = new Option("Lanao Del Norte","lanao-del-norte")
            document.getElementById('province').options[4] = new Option("Misamis Occidental","misamis-occidental")
            document.getElementById('province').options[5] = new Option("Misamis Oriental","misamis-oriental")
            break
        case "Region XI (DAVAO REGION)":
            removeOptions(document.getElementById('province'));
            document.getElementById('province').options[0] = new Option("","")
            document.getElementById('province').options[1] = new Option("Davao De Oro","davao-de-oro")
            document.getElementById('province').options[2] = new Option("Davao Del Norte","davao-del-norte")
            document.getElementById('province').options[3] = new Option("Davao Del Sur","davao-del-sur")
            document.getElementById('province').options[4] = new Option("Davao Occidental","davao-occidental")
            document.getElementById('province').options[2] = new Option("Davao Oriental","davao-oriental")
            document.getElementById('province').options[2] = new Option("Davao City","davao-city")
            break
        case "Region XII (SOCCSKSARGEN)":
            removeOptions(document.getElementById('province'));
            document.getElementById('province').options[0] = new Option("","")
            document.getElementById('province').options[1] = new Option("Cotabato","cotabato")
            document.getElementById('province').options[2] = new Option("Saragani","saragani")
            document.getElementById('province').options[3] = new Option("Sultan Kudarat","sultan-kudarat")
            document.getElementById('province').options[4] = new Option("South Cotabato","south-cotabato")
            break
        case "Region XIII (CARAGA)":
            removeOptions(document.getElementById('province'));
            document.getElementById('province').options[0] = new Option("","")
            document.getElementById('province').options[1] = new Option("Agusan Del Norte","agusan-del-norte")
            document.getElementById('province').options[2] = new Option("Agusan Del Sur","agusan-del-sur")
            document.getElementById('province').options[3] = new Option("Dinagat Islands","dinagat-islands")
            document.getElementById('province').options[4] = new Option("Surigao Del Norte","surigao-del-norte")
            document.getElementById('province').options[5] = new Option("Surigao Del Sur","surigao-del-sur")
            break
        case "ARMM":
            removeOptions(document.getElementById('province'));
            document.getElementById('province').options[0] = new Option("","")
            document.getElementById('province').options[1] = new Option("Basilan","basilan")
            document.getElementById('province').options[2] = new Option("Lanao Del Sur","lanao-del-sur")
            document.getElementById('province').options[3] = new Option("Maguindanao","maguindanao")
            document.getElementById('province').options[4] = new Option("Sulu","sulu")
            document.getElementById('province').options[5] = new Option("Tawi-Tawi","tawi-tawi")
            break                                
    }
    return true    
}

function dynamic_dropdoown_commodity(){
    var agricultural_product = agricultural_product_dropdown.options[agricultural_product_dropdown.selectedIndex].text
    switch (agricultural_product) {
        case "Beans and Legumes":
            removeOptions(document.getElementById('commodity'));
            document.getElementById('commodity').options[0] = new Option("","")
            document.getElementById('commodity').options[1] = new Option("Habitchuelas/Snap Beans","habitchuelas")
            document.getElementById('commodity').options[2] = new Option("Mongo/Mungbean","mongo")
            document.getElementById('commodity').options[3] = new Option("Peanut with Shell","peanut_w_shell")
            document.getElementById('commodity').options[4] = new Option("Peanut without Shell","peanut_wo_shell")
            document.getElementById('commodity').options[5] = new Option("Stringbeans","stringbeans")
            break
        case "Cereal":
            removeOptions(document.getElementById('commodity'));
            document.getElementById('commodity').options[0] = new Option("","")
            document.getElementById('commodity').options[1] = new Option("Rice Special","rice_special")
            document.getElementById('commodity').options[2] = new Option("Rice Premium","rice_premium")
            document.getElementById('commodity').options[3] = new Option("Well Milled Rice","well_milled_rice")
            document.getElementById('commodity').options[4] = new Option("Regular Milled Rice","regular_milled_rice")
            document.getElementById('commodity').options[5] = new Option("Corn Grain Yellow","corngrain_yellow")
            document.getElementById('commodity').options[6] = new Option("Corn Grain White","corngrain_white")
            document.getElementById('commodity').options[7] = new Option("Corn Grits Yellow","corngrits_yellow")
            document.getElementById('commodity').options[8] = new Option("Corn Grits White","corngrits_white")
            break
        case "Commercial":
            removeOptions(document.getElementById('commodity'));
            document.getElementById('commodity').options[0] = new Option("","")
            document.getElementById('commodity').options[1] = new Option("Coconut","coconut")
            break
        case "Condiments":
            removeOptions(document.getElementById('commodity'));
            document.getElementById('commodity').options[0] = new Option("","")
            document.getElementById('commodity').options[1] = new Option("Garlic","garlic")
            document.getElementById('commodity').options[2] = new Option("Ginger (Hawaiian)","ginger")
            document.getElementById('commodity').options[3] = new Option("Onion Red Creole (Bermuda Red)","onion_red_creole")
            document.getElementById('commodity').options[4] = new Option("Onion White (Yellow Granex)","onion_white")
            break
        case "Fruit Vegetables":
            removeOptions(document.getElementById('commodity'));
            document.getElementById('commodity').options[0] = new Option("","")
            document.getElementById('commodity').options[1] = new Option("Ampalaya or Bitter Gourd","ampalaya")
            document.getElementById('commodity').options[2] = new Option("Sayote or Chayote","chayote")
            document.getElementById('commodity').options[3] = new Option("Eggplant","eggplant")
            document.getElementById('commodity').options[4] = new Option("Squash","squash")
            document.getElementById('commodity').options[5] = new Option("Tomato","tomato")
            document.getElementById('commodity').options[6] = new Option("Upo or Bottle Gourd","upo")
            break
        case "Fruits":
            removeOptions(document.getElementById('commodity'));
            document.getElementById('commodity').options[0] = new Option("","")
            document.getElementById('commodity').options[1] = new Option("Banana (Lakatan)","banana_lakatan")
            document.getElementById('commodity').options[2] = new Option("Banana (Latundan)","banana_latundan")
            document.getElementById('commodity').options[3] = new Option("Banana (Saba)","banana_saba")
            document.getElementById('commodity').options[4] = new Option("Calamansi","calamansi")
            document.getElementById('commodity').options[5] = new Option("Mandarin Szinkom","mandarin_szinkom")
            document.getElementById('commodity').options[6] = new Option("Mango (Carabao)","mango_carabao")
            document.getElementById('commodity').options[7] = new Option("Mango (Piko)","mango_piko")
            document.getElementById('commodity').options[8] = new Option("Papaya","papaya_hawaiian")
            document.getElementById('commodity').options[9] = new Option("Pineapple","pineapple_hawaiian")
            break
        case "Fish":
            removeOptions(document.getElementById('commodity'));
            document.getElementById('commodity').options[0] = new Option("","")
            document.getElementById('commodity').options[1] = new Option("Alumahan or Indian Mackerel","mackerel_indian")
            document.getElementById('commodity').options[2] = new Option("Bangus or Milkfish","bangus")
            document.getElementById('commodity').options[3] = new Option("Bisugo or Threadfin Bream","bisugo")
            document.getElementById('commodity').options[4] = new Option("Alimasag or Blue Crab","crab_alimasag")
            document.getElementById('commodity').options[5] = new Option("Dalagang Bukid or Caesio","dalagang_bukid")
            document.getElementById('commodity').options[6] = new Option("Dilis or Anchovies","anchovies")
            document.getElementById('commodity').options[7] = new Option("Galunggong or Roundscad","galunggong")
            document.getElementById('commodity').options[8] = new Option("Sapsap or Slipmouth","sapsap")
            document.getElementById('commodity').options[9] = new Option("Suaje or Endeavor Prawn Shrimp","shrimpsuaje")
            document.getElementById('commodity').options[10] = new Option("Sugpo or Tiger Prawn Shrimp","shrimp_sugpo")
            document.getElementById('commodity').options[11] = new Option("Tilapia","tilapia")
            document.getElementById('commodity').options[12] = new Option("Tulingan or Frigate Tuna","frigate_tuna")
            break
        case "Leafy Vegetables":
            removeOptions(document.getElementById('commodity'));
            document.getElementById('commodity').options[0] = new Option("","")
            document.getElementById('commodity').options[1] = new Option("Cabbage","cabbage")
            document.getElementById('commodity').options[2] = new Option("Camote Tops","camote")
            document.getElementById('commodity').options[3] = new Option("Kangkong or Morning Glory","kangkong")
            document.getElementById('commodity').options[4] = new Option("Pechay","pechay")
            break
        case "Meat":
            removeOptions(document.getElementById('commodity'));
            document.getElementById('commodity').options[0] = new Option("","")
            document.getElementById('commodity').options[1] = new Option("Lean Beef","beef_lean_meat")
            document.getElementById('commodity').options[2] = new Option("Beef with Bones","beef_meet_w_bones")
            document.getElementById('commodity').options[3] = new Option("Lean Pork","pork_lean_meat")
            document.getElementById('commodity').options[4] = new Option("Pork with Bones","pork_meat_w_bones")
            document.getElementById('commodity').options[5] = new Option("Pork Pata","pork_pata")
            break
        case "Poultry":
            removeOptions(document.getElementById('commodity'));
            document.getElementById('commodity').options[0] = new Option("","")
            document.getElementById('commodity').options[1] = new Option("Chicken Fully Dressed (Broiler)","chicken_fully_dressed")
            document.getElementById('commodity').options[2] = new Option("Chicken Broiler","chicken_broiler")
            document.getElementById('commodity').options[3] = new Option("Chicken Egg","chicken_egg")
            document.getElementById('commodity').options[4] = new Option("Duck Egg","duck_egg")
            break
        case "Root Crops":
            removeOptions(document.getElementById('commodity'));
            document.getElementById('commodity').options[0] = new Option("","")
            document.getElementById('commodity').options[1] = new Option("Carrots","carrots")
            document.getElementById('commodity').options[2] = new Option("Gabi or Tar","gabi")
            document.getElementById('commodity').options[3] = new Option("Potato","potato")
            document.getElementById('commodity').options[4] = new Option("Kamote or Sweet Potato","sweet_potato")
            break
    }
    return true    
}
